//require
const LocalStrategy = require("passport-local").Strategy;
const bcrypt = require('bcryptjs');
var Users = require('./database').Users
var Items = require('./database').Items


//Setup local strategy

module.exports = function(app,passport){
   
//username and password perimeters to be passed in are from app.post function below
var authenticate = function(username, password, done) {


        Users.findOne({where: {'username': username
        }}).then(function(result) {
                if (!result) {
                    console.log("No such user found.")
                    return done(null, false);
                } else {
                    console.log("User found. Validating password.")
                    //TODO: add bcrypt later - bcrypt.compareSync(password, result.password)
                    if (bcrypt.compareSync(password, result.password)) {
                        console.log("Password valid")
                        /*var whereClause = {};
                        whereClause.email = username;
                        User
                            .update({ reset_password_token: null }, { where: whereClause });*/
                        return done(null, result);
                    } else {
                        console.log("User password invalid. Exiting...")
                        return done(null, false);
                    }
                }
            }).catch(function(err) {
                console.log("Server error")
                return done(err, false);
            });
};


passport.use(new LocalStrategy({
    usernameField: "username",
    passwordField: "password"},
    authenticate)
);

//to the session
passport.serializeUser(function(user,done){
    console.log("Serialising user.");
    done(null,user);
});

//to the request
passport.deserializeUser(function(user,done){
    //construct user
    console.log("Deserialising user.");

    Users.findOne({username: user.username})
    .then(function(result) {
        if (result) {
            done(null, user);
            console.log("User serialized to session and request.")
        }
    }).catch(function(err) {
        done(null, false);
        console.log("Error creating user.")
    });

});

}

/*
//username and password perimeters to be passed in are from app.post function below
function authenticate(username, password, done) {

    //grab user object from DB
    User.findOne({
        where: {
            email: username
        }
    }).then(function(result) {
        //if result is null exit
        if (!result) {
            return done(null, false);
        } else {
            //compares password (from form) and result password (from DB)
            if (bcrypt.compareSync(password, result.password)) {
                var whereClause = {};
                whereClause.email = username;
                User
                    .update({ reset_password_token: null }, { where: whereClause });
                return done(null, result);
            } else {
                return done(null, false);
            }
        }
    }).catch(function(err) {
        return done(err, false);
    });
}

app.post("/login", passport.authenticate("local", {
        successRedirect: HOME_PAGE,
        failureRedirect: "/",
        failureFlash : true
    }));

*/