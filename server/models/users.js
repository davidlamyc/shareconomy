module.exports = function (conn, Sequelize) {
    console.log("Entered mapping module.")
    var Users = conn.define("users",
        {   
            //ASSESSMENT REQ:a.	ID (INT) column that is the primary key and is auto-incrementing
            user_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            username: {
                type: Sequelize.STRING,
                allowNull: false
            },
            email: {
                type: Sequelize.STRING,
                allowNull: false
            },
            firstname: {
                type: Sequelize.STRING,
                allowNull: false
            },
            lastname: {
                type: Sequelize.STRING,
                allowNull: false
            },
            address: {
                type: Sequelize.STRING,
                allowNull: false
            },
            postal: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            password: {
                type: Sequelize.STRING,
                allowNull: false
            },
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false
         });

    return Users;
};
