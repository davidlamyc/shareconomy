module.exports = function (conn, Sequelize) {
    var Interest = conn.define("interest",
        {   
            //ASSESSMENT REQ:a.	ID (INT) column that is the primary key and is auto-incrementing
            interest_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            item_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            borrower_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            user_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            borrower_email: {
                type: Sequelize.STRING,
                allowNull: false
            },
            borrower_username: {
                type: Sequelize.STRING,
                allowNull: false
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false
         });

    return Interest;
};
