module.exports = function (conn, Sequelize) {
    var Items = conn.define("items",
        {
            item_id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            category: {
                type: Sequelize.STRING,
                allowNull: false
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            description: {
                type: Sequelize.STRING,
                allowNull: false
            },
            lent_status: {
                type: Sequelize.ENUM('yes','no'),
                allowNull: false
            },
            user_id: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            rate: {
                type: Sequelize.INTEGER,
            },
            imgurl: {
                type: Sequelize.STRING,
            },
            username: {
                type: Sequelize.STRING,
            }
        },
        {
            // don't add timestamps attributes updatedAt and createdAt
            timestamps: false
         });

    return Items;
};