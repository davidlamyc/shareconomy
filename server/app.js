const express = require("express");
const app = express();
const cookieParser = require('cookie-parser');
const bodyParser = require("body-parser");
const session = require("express-session");
const passport = require("passport");
const bcrypt = require('bcryptjs');
const multer = require("multer");
const path = require("path");
const mysql = require('mysql');
const mysql2 = require('mysql2');
const Sequelize = require("sequelize");
const flash = require("connect-flash");
const fs = require("fs");
const AWS = require('aws-sdk');
const LocalStrategy = require("passport-local").Strategy;
const PORT = process.env.PORT || 3000;

//entry point from index.html in client side folder
app.use(express.static(__dirname + "/../client"));

const storage = multer.diskStorage({
    destination: './upload_tmp',
    filename: function(req, file, callback){
        callback(null, Date.now() + '-' + file.originalName);
    }
}),
upload = multer({
    storage:storage
});

//body-parser
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//require auth.js
require('./auth')(app,passport);


/**************Interest/Mailgun****************/
var mailgun = require("mailgun-js");
var api_key = 'key-f2300e840b142e4ca56e6fe45d3b4fd3';
var DOMAIN = 'sandbox8435925928b846359d6b030616be0cb9.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: DOMAIN});


/*
app.post("/interest", function(req,res){
    var data = {
        from: 'Shareconomy Team <contact@shareconomy.org>',
        to: 'davidlam.yc@gmail.com',
        subject: 'Hi davidlam.yc, your item has gotten interest!' ,
        text: 'Hey davidlam.yc, ' + 
        req.body.username + ' is interested in your ' + req.body.name
        + '. Message: ' + req.body.message +
        '. Please reply to your prospective lender at ' + req.body.myemail +
        ' to negotiate your terms. Have a great day!'
        };
        mailgun.messages().send(data, function (error, body) {
        console.log("Email was sent:", body);
        });
})
*/

app.use("/img",
    express.static(path.join(__dirname, "/../upload_tmp")));

var multipart = multer({dest: path.join(__dirname, "/uploads/")});

var Users = require('./database').Users
var Items = require('./database').Items
var Interest = require('./database').Interest


/**************SESSION/PASSPORT****************/
//initialise session - configure session before enabling persistent login session
/*
app.use(session({
    secret: "shareconomy",
    saved: true,
    saveUninitialized: true
}));*/

app.use(session({
    secret: "shareconomy"
//    saved: true,
//    saveUninitialized: true
}));


//initialise passport
app.use(passport.initialize());
app.use(passport.session());

/************INTEREST*************/

//req.params contains interest_id only
app.delete('/api/interest/:interest_id', function(req, res){
    console.log("Deleting interest:", req.params.interest_id);
    Interest.destroy({where:{interest_id: req.params.interest_id}})
    .then(function(result){
        console.log("Interest deleted");
        res.sendStatus(202)
    }).catch(function(err){
        console.log("Error:", err)
    })
})

app.get('/api/interest/item/:item', function(req,res){
    console.log("Getting interest objects");
    console.log("Req.params:", req.params.item);
    Interest.findAll({where:{item_id: req.params.item}})
    .then(function(result){
        console.log(JSON.stringify(result));
        res.json(result)
    }).catch(function(err){
        console.log(err)
    })
})

app.post("/interest", function(req,res){
    Users.findOne({where: {user_id: req.body.user_id}})
        .then(function(result){
            var itemOwner = result;
            console.log("THIS USER START");
            console.log("User's id:", req.body.this_user_id);
            console.log("User's email:", req.body.this_user_email);
            console.log("User's address:", req.body.this_user_address);
            console.log("User's first:", req.body.this_user_firstname);
            console.log("User's last:", req.body.this_user_lastname);
            console.log("User's postall:", req.body.this_user_postal);
            console.log("User's username:", req.body.this_user_username);
            console.log("OWNER START");
            console.log("Category:", req.body.category);
            console.log("Item name:", req.body.name);
            console.log("Item id:", req.body.item_id)
            console.log("Description:", req.body.description);
            console.log("Rate:", req.body.rate);
            console.log("Message:", req.body.message);
            console.log("Item's user_id:", req.body.user_id);
            console.log("Item owner:", itemOwner.username)
            console.log("Owner name:", itemOwner.firstname);
            console.log("Owner name:", itemOwner.lastname);
            console.log("Owner email:", itemOwner.email)
            var interest = {
                item_id: req.body.item_id,
                borrower_id: req.body.this_user_id,
                user_id: itemOwner.user_id,
                borrower_email: req.body.this_user_email,
                borrower_username: req.body.this_user_username
                }
                //TODO: Server side validation
                Interest.create(interest, function(err, result){
                    if(result){
                    console.log("Interest created in DB.")
                    } else {
                    console.log("err is", err)
                    }
                }).then(function(result){
                    res.json(result);
                }).catch(function(e){
                    console.error(e);
                })
            var data = {
                from: 'Shareconomy Team on behalf of ' + req.body.this_user_firstname + '<contact@shareconomy.org>',
                to: itemOwner.email,
                subject: 'Hi ' + itemOwner.firstname + ', your item has gotten interest!' ,
                text: 'Hey ' + itemOwner.firstname  + ', ' +
                req.body.this_user_username + ' is interested in your ' + req.body.name
                + '. Message from requestor: ' + req.body.message +
                '. Please reply to your prospective lender at ' + req.body.this_user_email +
                ' to negotiate your terms. Have a great day!'
            };/*
            mailgun.messages().send(data, function (error, body) {
            console.log("Email was sent:", body);
        });*/
        
        }).catch(function(err){
        console.log(err)
        })
})

/**************ITEM API****************/

app.delete("/api/item/:item_id", function(req,res){
    console.log("Deleting item by category:", req.params.item_id);
    Items.destroy({where:{item_id: req.params.item_id}})
    .then(function(result){
        console.log("Interest deleted");
        res.sendStatus(202);
    }).catch(function(e){
        console.error(e);
    })
})

app.put("/api/item/:item_id", function(req,res){
    console.log("Item object:", req.body);
    console.log("Item id:", req.body.item_id);
    console.log("Item id:", req.body.lent_status);
    Items.update({
        lent_status: req.body.lent_status,
    }, {where :{item_id: req.body.item_id}})
    .then(function(result){
        console.log("Updated", result)
        res.sendStatus(200)
        })
        .catch(function(err){
            console.log(err)
        })
})

app.put("/api/user", function(req,res){
    console.log("USER FOR UPDATE:", req.body)
    var id = parseInt(req.body.user_id);
    console.log(typeof(id));
    Users.update({
        address: req.body.address,
        email: req.body.email,
        postal: req.body.postal,
    }, {where :{user_id: id}})
        .then(function(result){
            console.log("Updated", result)
            res
                .status(200)
        })
        .catch(function(err){
            console.log(err)
        })
});

app.get("/api/items", function(req,res){
    console.info("Retrieving items from DB");
    Items.findAll({})
    .then(function(result){
        res.json(result);
    }).catch(function(e){
        console.error(e);
    })
});

app.get("/api/items/:category", function(req,res){
    console.log("Retrieving items by category:", req.params.category)
    Items.findAll({
        where: {category: req.params.category}     
    }).then(function(result){
        res.json(result);
    }).catch(function(e){
        console.error(e);
    })
});

//REPLACE THIS
app.get("/api/items/user/:user_id", function(req,res){
    console.log("Retrieving items from user id:", req.params.user_id)
    Items.findAll({where: {user_id: req.params.user_id}})
    .then(function(result){
        res.json(result)
    }).catch(function(e){
        console.error(e);
    })
});

app.post("/api/items", upload.single("imgFile"), function(req, res){
    console.log("req.file.originalname",req.file.originalname);
    console.log("req.file.mimetype",req.file.mimetype);
    console.log("req.file.filename",req.file.filename);
    console.log("req.file.path",req.file.path);
    console.log("req.file.size",req.file.size);
    console.log("req.body.username",req.body.username)
    console.info("Non file fields to be posted", req.body);
    var item = {
        user_id: req.body.user_id,
        category: req.body.category,
        name: req.body.name,
        description: req.body.description,
        rate: req.body.rate,
        lent_status: "no",
        imgurl: req.file.filename,
        username: req.body.username
    }
    //TODO: Server side validation
    Items.create(item, function(err, result){
        if(result){
        console.log("Item created in DB.")
        } else {
        console.log("err is", err)
        }
    }).then(function(result){
        res.json(result);
    }).catch(function(e){
        console.error(e);
    })
});

/***************USER API****************/

//Checks for req.user serialised by passport and stored in the session
app.get("/api/user/status", function(req,res){
    console.log("Waiting for deserialisation to complete.")
    if (req.user){
        console.log("Passing user to client:", req.user);
        res.send(req.user);
    } else {
        console.log("No user found. Req.user empty");
    }
});

app.get("/api/users", function(req,res){
    console.log("Getting all users")
    Users.findAll({})
    .then(function(result) {
        console.log("users received:", result)
        res
            .status(200)
            .json(result);
    }).catch(function(err) {
        console.log("Error retrieving users.")
    });
});

app.get("/api/users/:username", function(req,res){
    console.info("Retrieving items from DB from username", req.params.username);
    Users.findOne({where: {username: req.params.username}}).then(function(result){
        res
            .status(200)
            .json(result);
    })
    .then(function(result){
        console.log(result)
    }).catch(function(err){
        console.log(err)
    })
});

//Delete user
app.delete("/api/user", function(req,res){
    console.log("Deleting...", req.body)
    Users.destroy({where: {username: "deleteuser"}})
    .then(function(result){
        console.log("user destroyed.")
    }).catch(function(e){
            console.error(e);
    })
})

app.put("/api/user", function(req,res){
    console.log("USER FOR UPDATE:", req.body)
    var id = parseInt(req.body.user_id);
    console.log(typeof(id));
    Users.update({
        address: req.body.address,
        email: req.body.email,
        postal: req.body.postal,
    }, {where :{user_id: id}})
        .then(function(result){
            console.log("Updated", result)
            res
                .status(200)
        })
        .catch(function(err){
            console.log(err)
        })
});



/****************REGISTER******************/
app.post("/register", function(req, res){
    console.log("Creating user with details:", req.body)
    var hashpassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null)
    var user = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        username: req.body.username,
        email: req.body.email,
        password: hashpassword,
        address: req.body.address,
        postal: req.body.postal
    }
    Users.create(user, function(err, result){
        if(result){
        console.log("User created in DB.")
        } else {
        console.log("err is", err)
        }
    }).then(function(result){
            res.status(200).json(result);
        }).catch(function(e){
            console.error(e);
        })
});


/****************LOGIN/LOGOUT****************/
app.post("/login",
    function(req,res,next){
        passport.authenticate('local',
            // passport is server-side execution. De-coupling server-side redirects removes
            // conflict with client-side handling. Just return success/fail to client
            // {
            //     successRedirect: "/#!/list",    // pointing to $state defined URL: /#!/list
            //     failureRedirect: "/#!/login"    // pointing to $state defined URL: /#!/login
            // }
            //
            // passport Documentation
            // If an exception occurred, 'err' will be set.
            // If authentication failed, 'user' will be set to false
            // 'info' contains optional details provided by strategy verify callback
            function(err, user, info) {
                // general server-side exception error
                if (err) {
                    return next(err);
                };
                // if authentication failed: invalid email or password
                if (!user) {
                    return res.sendStatus(401);
                };
                // passport login() call to establish a login session.
                // authenticate() invokes req.login() automatically
                // Primarily used when users sign up, during which req.login() can be invoked
                // to automatically log in newly registered user.
                req.login(user, function(err) {
                    if (err) {
                        return res.sendStatus(500);
                    } else {
                        // values of 'user' will be assigned to req.user
                        res.sendStatus(200);
                    };
                });
        })(req,res,next)
    }
);

app.get("/logout", function(req, res) {
        req.logout(); // clears the passport session
        req.session.destroy(); // destroys all session related data
        res.send(req.user).end();
});

app.listen(PORT,function(){
    console.log("Server started at", PORT)
})