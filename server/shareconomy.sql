DROP DATABASE IF EXISTS shareconomy;
CREATE DATABASE IF NOT EXISTS shareconomy;
USE shareconomy;

CREATE TABLE users (
    user_id     INT         	NOT NULL,
	username   	VARCHAR(25)   	NOT NULL,
    email  		VARCHAR(55)     NOT NULL,
    password    VARCHAR(255)    NOT NULL,
    first_name  VARCHAR(55)     NOT NULL,
    last_name   VARCHAR(55)     NOT NULL,    
	address		VARCHAR(255) 	NOT NULL,
	postal 		INT(6) 			NOT NULL,
	PRIMARY KEY (user_id)
);

CREATE TABLE items (
	item_id		INT				NOT NULL,
    category    VARCHAR(25)     NOT NULL,
    name   		VARCHAR(55)     NOT NULL,
    description VARCHAR(255),
	lent_status ENUM ('yes','no') NOT NULL,
	user_id		INT				NOT NULL,
	rate		INT(6),
	PRIMARY KEY (item_id),
	FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE,
);

CREATE TABLE interests (
	interest_id 	INT				NOT NULL,
	item_id			INT				NOT NULL,
    borrower_id 	INT, 
    user_id			INT     		NOT NULL,
    
	PRIMARY KEY (item_id, interest_id),
	FOREIGN KEY (item_id) REFERENCES items (item_id) ON DELETE CASCADE,
);

CREATE TABLE borrowing (
	interest_id INT				NOT NULL,
	item_id		INT				NOT NULL,
    borrower_id	INT     		NOT NULL, 
    user_id		INT     		NOT NULL, 
    
	PRIMARY KEY (item_id),
	FOREIGN KEY (item_id) REFERENCES items (item_id) ON DELETE CASCADE,
);


1.Interest view
ng-repeat based on a query for interest(i.e. 3 users want 1 item = 3 display items). Where user_id: user_id and borrow_id = null, display interest ID. When user clicks accept, borrow_id is populated.
Go into item and turn lent status to yes

2.Profile view #1
ng-repeat based on borrower_id
(Once other user accepts, this is populated with user_id of the borrower, profile view grabs all this, populates the view)

3. profile view #2
When item lent out gets flick to not lent out, it should disappear from borrowing view