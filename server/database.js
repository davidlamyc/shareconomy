const Sequelize = require("sequelize");

//sequelize config
const MYSQL_USERNAME = 'David';
const MYSQL_PASSWORD = 'highway1';

var sequelize = new Sequelize(
    'shareconomy',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: 'localhost', // default port    : 3306
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

var Users = require('./models/users')(sequelize, Sequelize);
var Items = require('./models/items')(sequelize, Sequelize);
var Interest = require('./models/interest')(sequelize, Sequelize);

//ASSESSMENT REQ: FOREIGN KEY FOR JOINING TABLES
//Users.hasMany(Items, {foreignKey:'user_id'})

module.exports = {
    Users: Users,
    Items: Items,
    Interest: Interest
}
