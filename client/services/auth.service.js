//TODO: fixed error handling for wrong password and no user found.
(function(){

angular
    .module("ShareApp")
    .service('AuthService',['$rootScope', '$http', '$q', AuthService]);

    //Post not required as passport handle pulling in username and password
    function AuthService($rootScope, $http, $q){
        var authService = this;

        authService.userLogIn = false;
        authService.user = null;

        //check if user is logged in
        authService.getUserStatus = function(){
            console.log("Getting user status.")
            return $http.get('/api/user/status')
            .then(function(result){
                if (result.data){
                    //console.log("User returned from server:", result.data);
                    $rootScope.$broadcast("sendUser", result.data);
                    authService.user = result.data;
                    authService.userLogIn = true;
                } else {
                    console.log("Req.user not found. No user logged in");
                    authService.userLogIn = false;
                }  
            })
            .catch(function(err){
                console.log("Error!", err)
            })
        }

        authService.login = function(user){
            //console.log("authService: Authenticating user.")
            var deferred = $q.defer();
            // send a post request to the server
            $http.post('/login', user)
                .then(function(data) {
                    console.log("Authentication success. Status code:", data.status);                  
                    authService.getUserStatus()
                    deferred.resolve();
                })
                .catch(function (err) {
                    console.log("Login error")
                    user = false;
                    deferred.reject();
                })
                // return promise object
                return deferred.promise;
            }

        authService.logout = function(){

            // create a new instance of deferred
            var deferred = $q.defer();

            // send a get request to the server
            $http.get('/logout')
            // handle success
                .then(function (data) {
                    authService.userLogIn = false;
                    authService.user = null;
                    deferred.resolve();
                })
                // handle error
                .catch(function (data) {
                    deferred.reject();
                });
                // return promise object
                return deferred.promise;
            }


    }
})();
