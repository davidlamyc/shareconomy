//TODO: fixed error handling for wrong password and no user found.
(function(){

angular
    .module("ShareApp")
    .service('ContactService',['$rootScope', '$http', '$q', '$state', ContactService]);

    //Post not required as passport handle pulling in username and password
    function ContactService($rootScope, $http, $q, $state){
        var contactService = this;

        contactService.item = {};

        contactService.contactLender = function(index){
            contactService.item = index;
        }
    }
})();
