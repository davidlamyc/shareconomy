(function(){
    angular
        .module("ShareApp")
        .controller("LendCtrl", ["$http", "$httpParamSerializerJQLike", "$state", "Upload", "Flash", "AuthService", LendCtrl]);

    function LendCtrl($http,$httpParamSerializeJQLike,$state,Upload,Flash,AuthService){
        var lendCtrl = this;

        lendCtrl.item = {};
        lendCtrl.imgFile = "";
        lendCtrl.user = AuthService.user

        lendCtrl.successAlert = function(){
            var message = 'Thank you for lending to the community!';
            var id = Flash.create('success', message, 4000, {class: 'custom-class', id: 'custom-id'}, true);
        }

        lendCtrl.enterItem = function (){
            Upload.upload({
                url:"/api/items",
                data:{
                    imgFile: lendCtrl.item.imgFile,
                    user_id: lendCtrl.user.user_id,
                    category: lendCtrl.item.category,
                    name: lendCtrl.item.name,
                    description: lendCtrl.item.description,
                    rate: lendCtrl.item.rate,
                    username: lendCtrl.user.username
                }
            })
            .then(function(result) {
                lendCtrl.successAlert()
                $state.go("borrow");
            })
            .catch(function (err) {
                console.log("error")
            });
        }
    };

})();
