//David[3/10]: Work in progress
(function(){
    angular
        .module("ShareApp")
        .controller("BorrowCtrl", ["$http", "$httpParamSerializerJQLike", "$state", "$scope", "$rootScope", "Flash", "ContactService", "AuthService", BorrowCtrl]);

    function BorrowCtrl($http,$httpParamSerializeJQLike, $state, $scope, $rootScope, Flash, ContactService, AuthService){
        var borrowCtrl = this;
        borrowCtrl.items = [];
        borrowCtrl.category = {};
        borrowCtrl.contactItem = "";
        borrowCtrl.loginCheck = false;

        borrowCtrl.registerAlert = function(){
            var message = 'Please login or register to contact lender!';
            var id = Flash.create('danger', message, 3000, {class: 'custom-class', id: 'custom-id'}, true);
        }

        borrowCtrl.getItems = function(){
            console.info("Getting items from DB.");
            $http.get("/api/items")
                .then(function(result){
                    borrowCtrl.items = result.data;
                })
                .catch(function(err){
                    console.log("Error retrieving DB data.")
                })
            }

        borrowCtrl.getItems();

        borrowCtrl.getByCategory = function(category){
            console.info("Getting item from DB.");
            $http.get("api/items/" + category)
                .then(function(result){
                    borrowCtrl.items = result.data;
                })
                .catch(function(err){
                    console.log("Error retrieving DB data.")
                })
            }
            
        borrowCtrl.contactLender = function(item){
            if (/*borrowCtrl.loginCheck*/ AuthService.user){
                ContactService.contactLender(item);
                $state.go("contact")
            } else {
                borrowCtrl.registerAlert();
            }
        }

        $scope.$on("sendUser", function(evt,data){
            if (data){
                borrowCtrl.loginCheck = true;
            } else {
                console.log('Error');
            }
        });
    }

})();
