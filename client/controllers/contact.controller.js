(function(){
    angular
        .module("ShareApp")
        .controller("ContactCtrl", ["$http", "$httpParamSerializerJQLike", "$state", "$scope", 
        "ContactService", "Flash", "AuthService", ContactCtrl]);

    function ContactCtrl($http,$httpParamSerializeJQLike,$state,$scope,ContactService,Flash,AuthService){
        var contactCtrl = this;

        contactCtrl.successAlert = function(){
            var message = 'Success! A mail has been sent to the lender of the item.';
            var id = Flash.create('success', message, 5000, {class: 'custom-class', id: 'custom-id'}, true);
        }

        contactCtrl.item = ContactService.item;
        contactCtrl.user = AuthService.user

        contactCtrl.sendInterest = function (){
            $http({
                url: "/interest",
                method: "POST",
                data: $httpParamSerializeJQLike({
                    //this user
                    this_user_id: contactCtrl.user.user_id,
                    this_user_email: contactCtrl.user.email,
                    this_user_address: contactCtrl.user.address,
                    this_user_firstname: contactCtrl.user.firstname,
                    this_user_lastname: contactCtrl.user.lastname,
                    this_user_postal: contactCtrl.user.postal,
                    this_user_username: contactCtrl.user.username,
                    //
                    item_id: contactCtrl.item.item_id,
                    user_id: contactCtrl.item.user_id,
                    category: contactCtrl.item.category,
                    name: contactCtrl.item.name,
                    description: contactCtrl.item.description,
                    rate: contactCtrl.item.rate,
                    message: contactCtrl.item.message
                }),
                headers:{
                        "Content-Type": "application/x-www-form-urlencoded"
                }
            })
            .then(function(result) {
                contactCtrl.successAlert();
                console.info("Sent to mailgun");
            })
            .catch(function (err) {
                console.error("error")
            });
        }

    }

})();
