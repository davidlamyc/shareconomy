//David[3/10]: Work in progress
(function(){
    angular
        .module("ShareApp")
        .controller("ProfileCtrl", ["$http", "$httpParamSerializerJQLike", "$state", "$q", ProfileCtrl]);

    function ProfileCtrl($http,$httpParamSerializeJQLike,$state,$q){
        var profileCtrl = this;

        profileCtrl.user = {};
        profileCtrl.items = [];

        profileCtrl.deleteItem = function(item){
            $http.delete("/api/item/" + item.item_id)
            .then(function(result){
                for (i = 0; i < profileCtrl.items.length; i++) {
                   // console.log("profileCtrl.items[i].item_id:", profileCtrl.items[i].item_id)
                    if (item.item_id == profileCtrl.items[i].item_id){
                        //console.log("Deleting:", profileCtrl.items[i]);
                        profileCtrl.items.splice(i, 1);
                    }
                }
            }).catch(function(err){
                console.log("Err is", err)
            })
        }

        profileCtrl.deleteInterest = function(interest, index){
            //console.log("Deleting interest_id", interest.interest_id);
            //console.log("Associated item_id", interest.item_id);
            var deferred = $q.defer()
            $http.delete("/api/interest/" + interest.interest_id)
            .then(function(result){
                //console.log("Interest successfully deleted. Status:", result.status);
                //match interest.item_id with correct iem
                for (i = 0; i < profileCtrl.items.length; i++) {
                    if (interest.item_id == profileCtrl.items[i].item_id){
                        //remove interest item where based on index
                        //console.log("Interest ID to be deleted:", profileCtrl.items[i].interest[index].interest_id);
                        var targetItem = profileCtrl.items[i].interest;
                        targetItem.splice(index, 1);
                    }
                }
            }).catch(function(err){
                deferred.reject(err);
                //console.log("Error deleting interest.")
            })
        }

        //Initiate user details
        profileCtrl.getUser = function(){
            var deferred = $q.defer();
            //console.log("Getting profile details...")
            $http.get('/api/user/status')
            .then(function(user){
                //console.log("User ID to fetch items:",user.data.user_id)
                profileCtrl.getUserItems(user.data.user_id);
                deferred.resolve(user);
                profileCtrl.user = user.data
                //console.log("User details:", profileCtrl.user)
            })
            .catch(function(err){
                deferred.reject(err);
                console.log(err)
            })
            return deferred.promise
        }
        profileCtrl.getUser();

        profileCtrl.updateLentStatus = function(item){
            //console.log("item:", item);
            //console.log("item status:", item.lent_status);
            //console.log("item_id:", item.item_id);
            $http.put("/api/item/" + item.item_id, item)
            .then(function(result){
                console.log(result)
            }).catch(function(err){
                console.log(err)
            });
        }

        //initialise items user is lending   
        profileCtrl.getUserItems = function(userID){
            var deferred = $q.defer();
            var idx = 0;
            //console.log("Getting items by user_id", userID)
            $http.get('/api/items/user/'+ userID)
            .then(function(items){
                deferred.resolve(items);
                profileCtrl.items = items.data
                console.log("User items retrieved:", profileCtrl.items)
                var itemIdArray = [];
                profileCtrl.items.forEach(function(item){
                    var item_id = item.item_id;
                    itemIdArray.push(item_id);
                })
                //console.log(itemIdArray);
                itemIdArray.forEach(function(item_id){
                    //console.log("Fetching interest for item_id:", item_id)
                    $http.get('/api/interest/item/' + item_id)
                    .then(function(interest){
                        profileCtrl.items[idx].interest = interest.data;
                        //console.log("Interest item received:", profileCtrl.items)
                        idx =+ 1;
                    })
                    .catch(function(err){
                        console.log(err)
                    })
                })
            })
            .catch(function(err){
                deferred.reject(err);
                console.log(err)
            })
            var idx = 0;
            return deferred.promise
        }
    }
    
})();
