(function(){
    angular
        .module("ShareApp")
        .controller("LoginCtrl", ["$http", "$state", "Flash", "AuthService", LoginCtrl]);

    function LoginCtrl($http, $state, Flash, AuthService){
        var loginCtrl = this;

        loginCtrl.user = {};
        loginCtrl.status = "";

        loginCtrl.successAlert = function(){
            var message = 'Authentication successful.';
            var id = Flash.create('success', message, 2500, {class: 'custom-class', id: 'custom-id'}, true);
        }

        loginCtrl.failureAlert = function(){
            var message = 'Please use a valid password and username.';
            var id = Flash.create('danger', message, 2500, {class: 'custom-class', id: 'custom-id'}, true);
        }
        
        //passport picks up username and password from login HTML page
        loginCtrl.login = function(user){
            console.log("Authenticating user:", user.username);
            AuthService.login(user)
                .then(function(result){
                    loginCtrl.successAlert()
                    $state.go("borrow");
                }).catch(function(err){
                    loginCtrl.failureAlert();
                    $state.go("login");
                })
        }
    }

})();