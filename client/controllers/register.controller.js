(function(){
    angular
        .module("ShareApp")
        .controller("RegisterCtrl", ["$http", "$httpParamSerializerJQLike", "$state","Flash", RegisterCtrl]);

    function RegisterCtrl($http,$httpParamSerializeJQLike,$state, Flash){
        var registerCtrl = this;
        
        registerCtrl.user = {};

        
        registerCtrl.successAlert = function(){
            var message = 'Registration successful! Please proceed to log in.';
            var id =Flash.create('success', message, 5000, {class: 'custom-class', id: 'custom-id'}, true);
        }


        registerCtrl.submit = function(){
            console.info("Registering user...")
            $http({
                url: "/register",
                method: "POST",
                data: $httpParamSerializeJQLike({
                    firstname: registerCtrl.user.firstname,
                    lastname: registerCtrl.user.lastname,
                    username: registerCtrl.user.username,
                    email: registerCtrl.user.email,
                    password: registerCtrl.user.password,
                    address: registerCtrl.user.address,
                    postal: registerCtrl.user.postal
                }),
                headers:{
                        "Content-Type": "application/x-www-form-urlencoded"
                }
            })
            .then(function(res){
                //console.log("Registration successful:",res);
                registerCtrl.successAlert();
            })
            .catch(function(err){
                console.log(err)
            })
        }
    }

})();