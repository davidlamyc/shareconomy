(function () {
    angular
        .module("ShareApp")
        .controller("IndexCtrl", ["$http", "$q", "AuthService", "$scope", "$rootScope", "$state", IndexCtrl]);

    function IndexCtrl($http, $q, AuthService, $scope, $rootScope, $state){
        var indexCtrl = this;

        indexCtrl.username = null;
        indexCtrl.displaySwitch = false;

        AuthService.getUserStatus()

        indexCtrl.logout = function(){
            AuthService.logout()
            indexCtrl.username = null;
            indexCtrl.displaySwitch = false;
        }
        
        $scope.$on("sendUser", function(evt,data){
            indexCtrl.username = data.username;
            if (indexCtrl.username){
                indexCtrl.displaySwitch = true;
            } else {
                console.log('Error');
            }
        });
       
    }

})();
