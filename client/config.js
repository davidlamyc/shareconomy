(function(){
    angular
        .module("ShareApp")
        .config(ShareConfig);

    ShareConfig.$inject = [ "$stateProvider", "$urlRouterProvider" ];

    function ShareConfig($stateProvider, $urlRouterProvider){
        $stateProvider
            .state("register",{
                url:"/register",
                templateUrl:"/views/register.html"
            })
            .state("login",{
                url:"/login",
                templateUrl:"/views/login.html"
            })
            .state("lend",{
                url:"/lend",
                templateUrl:"/views/lend.html"
            })
            .state("borrow",{
                url:"/borrow",
                templateUrl:"/views/borrow.html",
            })
            .state("profile",{
                url:"/profile",
                templateUrl:"/views/profile.html"
            })
            .state("contact",{
                url:"/contact",
                templateUrl:"/views/contact.html"/*,
                resolve: {
                    authenticated: function (AuthService) {
                        console.log("authenticated ?");
                        console.log(AuthService.getUserStatus());
                        return AuthService.getUserStatus();
                    }
                }*/
            })
            .state("error",{
                url:"/error",
                templateUrl:"/views/error.html"
            })
            .state("admin",{
                url:"/admin",
                templateUrl:"/views/admin.html"
            })
            .state("user",{
                url:"/user",
                templateUrl:"/views/user.html"
            })
            .state("about",{
                url:"/about",
                templateUrl:"/views/about.html"
            })

        $urlRouterProvider.otherwise("/borrow");
    }
    
})();

